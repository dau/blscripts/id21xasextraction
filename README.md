# id21xasextraction

Batch extract of XANES spectra.

## Run Script

Activate the environment ones when you start a new terminal

```bash
source ~/software/envs/id21xasextraction/bin/activate
```

Run the script

```bash
python /data/id21/inhouse/scripts/id21xasextraction/xanes_extract_safe.py
```

The directory `/data/id21/inhouse/scripts/id21xasextraction/` depends
one where the repository is located.

## First time

If this is the first time you run the script on this computer,
you need to create a python environment and install the dependencies.

### Create Python Environment

```bash
mkdir -p ~/software/envs
python3 -m venv ~/software/envs/id21xasextraction
source ~/software/envs/id21xasextraction/bin/activate
pip install -U pip setuptools
```

### Install Dependencies

```bash
source ~/software/envs/id21xasextraction/bin/activate
pip install -r /data/id21/inhouse/scripts/id21xasextraction/requirements.txt
```
