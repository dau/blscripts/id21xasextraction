"""
Example for batch extract of XANES spectra
Dependencies: python -m pip install -r requirements.txt

The extracted data has the following meaning

  | Classification Term | Orange Term   | Pandas DataFrame | CSV Table      | Description                              |
  |---------------------|---------------|------------------|----------------|------------------------------------------|
  | Sample              | Instance      | data             | row            | XANES scan                               |
  | Feature             | Variable      | column           | column         | XANES energy or motor position           |
  | Meta Feature        | Meta Variable | index            | special column | scan name, sample tags or subsample tags |
"""

import re
import os
import json
import h5py
import numpy as np
import pandas as pd
from pprint import pprint
import matplotlib.pyplot as plt
from collections import OrderedDict
from contextlib import contextmanager
from scipy.interpolate import Akima1DInterpolator
from PyMca5.PyMcaPhysics.xas.XASNormalization import XASNormalization

# fmt: off

### Input data for this code
mainh5path  = "/data/visitor/es1473/id21/20240502/RAW_DATA/es1473_id21.h5" # Path and name of the main h5 file
outpath     = "/tmp/wdntest/" # Path to save all the ouput data ("." is the current working directory)
matchers    = ["CuO_1"]  # Filter to fit files with the matching name (e.g. "sample_1" or "region3"). Leave empty ("") to fit all
elemdet     = "fx2_det0_CuKa"  # Select the element and detector with data. Trans: "idet"; Fluor: "fx2_CuKa" fx2_MnKa
elemdetnorm = "CuKa_corr_norm0"  # Select the element and counter with the normalization and dead time correction. Trans: "absorp1"; Fluor: "CuKa_corr_norm0"
energycntr  = "energy_enc"  # Select the energy counter 

# Saving options
save_singles  = "no"  # Write "yes" to save each XANES spectra in a single .dat file for each of them "no" otherwise
save_average  = "no"  # Write "yes" to create files with the average of poi with several scans "no" otherwise
save_orange   = "yes"  # Write "yes" to create a single .csv file with all XANES spectrum "no" otherwise
orange_name   = "FluorescenceCu_es1473_orange"  # Name for the orange output file
save_excel    = "yes"  # Write "yes" to create a single .csv file with all XANES spectrum in columns layout "no" otherwise
excel_name    = "FluorescenceCu_es1473_excel"  # Name for the excel output file
add_positions = "no"  # Write "yes" to add the motor positions and "Jump" value to the CSV data, "no" otherwise
add_tags      = "yes"  # Write "yes" to add the sample and sub-sample tags to the CSV data, "no" otherwise

# Extra options for filtering XANES spectra
wlbkgratio = 0.05 # Ratio for the filter between the max of the white line and the mean of the pre edge background
nopoibckg  = 10 # Number of points to take the mean of the background level
trhnpoi    = 10 # Treshold to filter the data with less number of points in the spectrum than trhdnpoi

# Interpolation 
interpolate   = "yes"   # Write "yes" to perform an interpolation for every XANES spectrum "no" otherwise
energy_start  = 8.95 # First energy value of the XANES spectra in keV
energy_finish = 9.1 # Last energy value of the XANES spectra in keV
energy_step   = 0.0005  # Energy step size for the final interpolation spectra in keV

# Options for filtering experiments with more than one element (Cr, Mn, Fe, etc) or to cut the spectrum range
two_plus_elem   = "no"  # Write "yes" if you have XANES data for more than one element "no" otherwise
lwr_lmt_energy  = 2.401 # Lower limit for the energy of the XANES spectra to cut (this limit has to be inside the range of the experimental XANES)
hghr_lmt_energy = 2.56 # Higher limit for the energy of the XANES spectra to cut (this limit has to be inside the range of the experimental XANES)

# Normalization values
norm_choosing = "no" # Write "yes" to have all XANES data normalized using PyMca functions "no" otherwise 
edge = 9.0160     # Values taken from the PyMca GUI, XAS normalization option
pre  = "Linear", -0.036, -0.01188
post = "Linear", 0.0946, 0.179

# fmt: on

# ===== Start of all the functions for this code =====


def getscanlist():
    with h5py.File(mainh5path, mode="r", locking=False) as nxroot:
        dirname = os.path.dirname(nxroot.file.filename)
        scanlist = OrderedDict()
        for name in list(nxroot.keys()):
            link = nxroot.get(name, getlink=True)
            filename = os.path.join(dirname, link.filename)
            scanlist[name] = filename, link.path
        return scanlist


def iterscans():
    nxroot = None
    open_filename = None
    try:
        for scan_id, (filename, data_path) in scanlist.items():
            if filename != open_filename:
                try:
                    nxroot = h5py.File(filename, mode="r", locking=False)
                except Exception as e:
                    print(f"Cannot access file '{filename}' ({e})")
                    break
            try:
                scan = nxroot[data_path]
            except Exception as e:
                print(f"Cannot access scan '{filename}::{data_path}' ({e})")
                break
            yield scan_id, scan
    finally:
        if nxroot is not None:
            nxroot.close()


def fileselection():
    counter = 0
    xanesinseq = []
    counterseq = 0
    for scan_id, nxroot in iterscans():
        # print("file name as list = ", scan_id)
        try:
            all_counters = list(nxroot["measurement"])
            datacheck = str(nxroot["title"][()])
        except Exception as ex:
            print("The error exception in fileselection() is = ", ex)
            bad_scan_ids.append(scan_id)
            continue

        if "sequence_of_scans" in datacheck:
            counterseq += 1
            xanesinseq.append(counter)
            counter = 0
            # print("SEQUENCE = ", datacheck)
        # print("file name = ", all_data_files)
        if (elemdet in all_counters) and (energycntr in all_counters):
            counter += 1
            good_scan_ids.append(scan_id)
        else:
            bad_scan_ids.append(scan_id)

    xanesinseq.append(counter)
    xanesinseq = xanesinseq[1:]
    # print("List with number of XANES from each sequence scan = ", xanesinseq)
    return xanesinseq


def fileselectiontags():
    counter = 0
    xanesinseq = []
    counterseq = 0

    for scan_id, nxroot in iterscans():
        try:
            datacheck = str(nxroot["title"][()])
            # print("XANES or sequence of scans check = ", datacheck)
        except Exception as ex:
            print("The error exception in fileselectiontags() is = ", ex)
            bad_scan_ids.append(scan_id)
            continue

        if "sequence_of_scans" in datacheck:
            counterseq += 1
            xanesinseq.append(counter)
            counter = 0
            # print("SEQUENCE = ", datacheck)
        elif (
            ("zaptraj enetraj" in datacheck)
            or ("ascan enmonound" in datacheck)
            or ("trigscan" in datacheck)
        ):  # change here
            counter += 1
            # print("XANES = ", datacheck)
            good_scan_ids.append(scan_id)

    xanesinseq.append(counter)
    xanesinseq = xanesinseq[1:]
    # print("List with number of XANES from each sequence scan = ", xanesinseq)
    return xanesinseq


@contextmanager
def openscan(scan_id):
    filename, scanname = scanlist[scan_id]
    with h5py.File(filename, mode="r", locking=False) as nxroot:
        yield nxroot[scanname]


def readspectrum(scan_id):
    with openscan(scan_id) as nxentry:
        data = nxentry["measurement"]
        if elemdetnorm in data:
            names = [energycntr, elemdetnorm]
        elif (
            energycntr
            and elemdet
            and "iodet"
            and "fx2_det0_fractional_dead_time" in data
        ):
            # print("Elemdetnorm not found, corr_norm done in this code = ", scan_id)
            names = [energycntr, elemdet, "iodet", "fx2_det0_fractional_dead_time"]
        else:
            print(f"Element '{elemdetnorm}' not found and not normalized = ", scan_id)
            names = [energycntr, elemdet]

        npoints = min(data[name].shape[0] for name in names)
        energy = data[energycntr][:npoints]

        # Checking for element correction normalization
        if elemdetnorm in data:
            spectrum = data[elemdetnorm][:npoints]
            # print("element Ka norm present")
        elif (
            energycntr
            and elemdet
            and "iodet"
            and "fx2_det0_fractional_dead_time" in data
        ):
            iodet = data["iodet"][:npoints]
            # print('iodet = ', iodet)
            det0 = data[elemdet][:npoints]
            # print('det0 = ', det0)
            det0_dead_time = data["fx2_det0_fractional_dead_time"][:npoints]
            # print('det0_dead_time = ', det0_dead_time )
            absorb_norm0 = det0 / (iodet * (1 - det0_dead_time))
            # print('absorb_norm0 = ', absorb_norm0)
            spectrum = absorb_norm0
            # print('spectrum = ', spectrum)
        else:
            spectrum = data[elemdet][:npoints]

    return energy, spectrum


def read_positioners(scan_id):
    if "AVG_" in scan_id:
        scan_id = scan_id[4:]
    with openscan(scan_id) as nxentry:
        positioners_val = nxentry["instrument/positioners"]
        samy = positioners_val["samy"][()]
        samz = positioners_val["samz"][()]
        sampy = positioners_val["sampy"][()]
        sampz = positioners_val["sampz"][()]
    return samy, samz, sampy, sampz


def read_tags(scan_id):
    if "AVG_" in scan_id:
        scan_id = scan_id[4:]
    with openscan(scan_id) as nxentry:
        sample_name = read_h5string(nxentry, "sample/name")
        notes = read_h5string(nxentry, "sample/notes/notes")
        return compile_tags(notes, sample_name)


def read_h5string(parent, name):
    try:
        dset = parent[name]
    except KeyError:
        return ""
    return dset[()].decode()


def compile_tags(notes, sample_name):
    if notes:
        notes = json.loads(notes)
        sample_tags = notes.get("tags")
        if sample_tags is None:
            sample_tags = list()
        subsample_notes = notes.get("subsample")
        if subsample_notes is None:
            subsample_notes = dict()
        subsample_tags = subsample_notes.get("tags")
        if subsample_tags is None:
            subsample_tags = list()
    else:
        sample_tags = list()
        subsample_tags = list()

    if sample_name not in sample_tags:
        sample_tags = [sample_name] + sample_tags
    sample_tags_as_str = ",".join(sorted([s.strip() for s in sample_tags]))
    subsample_tags_as_str = ",".join(sorted([s.strip() for s in subsample_tags]))

    if not sample_tags_as_str:
        sample_tags_as_str = "-"
    if not subsample_tags_as_str:
        subsample_tags_as_str = "-"
    return sample_tags_as_str, subsample_tags_as_str


def averaging():
    n = -1
    avgenergylist = []
    avgspectrumlist = []
    avgscanidlist = []
    print("")
    # print("All scans names in the main h5 file = ", len(xanesinseq))
    # print("Number of repeats on each scan = ", xanesinseq)

    for i in range(len(xanesinseq)):
        # print("i = ", i)
        avgspectrum = [0]
        for j in range(xanesinseq[i]):
            try:
                # print("j = ", j)
                n += 1
                # print("Next file is = ", scan_ids_foravg[n])
                energy, spectrum = readspectrum(scan_ids_foravg[n])
                if len(avgspectrum) > 1:
                    minno = min(len(avgspectrum), len(spectrum), len(energy))
                    avgspectrum = avgspectrum[:minno]
                    spectrum = spectrum[:minno]
                    energy = energy[:minno]
                avgspectrum = np.add(avgspectrum, spectrum)
            except Exception as ex:
                print("The error exception in averaging() is = ", ex)
                continue

        if not int(xanesinseq[i]) <= 1:
            avgspectrum = avgspectrum / (int(xanesinseq[i]))
            avg_scan_id = "AVG_" + scan_ids_foravg[n]
            avgenergylist.append(energy)
            avgspectrumlist.append(avgspectrum)
            avgscanidlist.append(avg_scan_id)
            # plot(energy, avgspectrum, avg_scan_id) # Plotting each XANES averaged spectra
    return avgenergylist, avgspectrumlist, avgscanidlist


def validatespectrum():
    validated = None
    if (
        (len(energy) < trhnpoi)
        or (len(spectrum) < trhnpoi)
        or (all(elem == spectrum[5] for elem in spectrum))
    ):
        print(
            "(Energy or spectrum) < (trhdnpoi or filter_for_energy) or all elements are the same"
        )
        validated = False

    choosing = max(spectrum) / np.mean(spectrum[2:nopoibckg])
    choosing = abs(choosing)
    print("Edge/background ratio = ", choosing)
    # print("sigma", (spectrum[:nopoibckg]).std())

    if choosing >= wlbkgratio:
        validated = True
    return validated


def plot(energy, spectrum, filename):
    plt.figure().suptitle(filename)
    plt.plot(energy, spectrum)
    plt.show()


def normalization(energy, spectrum, edge, pre, post):
    polynomial_map = {
        "Modif. Victoreen": -2,
        "Victoreen": -1,
        "Constant": 0,
        "Linear": 1,
        "Parabolic": 2,
        "Cubic": 3,
    }
    prename, premin, premax = pre
    postname, postmin, postmax = post

    pre_edge_regions = [[premin, premax]]
    post_edge_regions = [[postmin, postmax]]
    algorithm = "polynomial"
    algorithm_parameters = {
        "pre_edge_order": polynomial_map[prename],
        "post_edge_order": polynomial_map[postname],
    }

    result = XASNormalization(
        spectrum=spectrum,
        energy=energy,
        edge=edge,
        pre_edge_regions=pre_edge_regions,
        post_edge_regions=post_edge_regions,
        algorithm=algorithm,
        algorithm_parameters=algorithm_parameters,
    )
    (
        energy,
        normalizedSpectrum,
        edge,
        jump,
        pre_edge_function,
        prePol,
        post_edge_function,
        postPol,
    ) = result
    preLine = pre_edge_function(prePol, energy)
    postLine = post_edge_function(postPol, energy)
    return normalizedSpectrum, jump, preLine, postLine


def savedata(energy, spectrum, scan_id):
    combined = np.vstack((energy, spectrum)).T
    np.savetxt(os.path.join(outpath, scan_id + ".dat"), combined, fmt="%10.8f")


# Matching the energies for the interpolation and the cutting
if two_plus_elem == "yes":
    if interpolate == "yes":
        if energy_start <= lwr_lmt_energy:
            energy_start = lwr_lmt_energy + energy_step
        if energy_finish > hghr_lmt_energy:
            energy_finish = hghr_lmt_energy
save_singles = save_singles.lower()
save_average = save_average.lower()
save_orange = save_orange.lower()
save_excel = save_excel.lower()
add_positions = add_positions.lower()
norm_choosing = norm_choosing.lower()
add_tags = add_tags.lower()

# File selection and starting lists
good_scan_ids = []
bad_scan_ids = []
invalid_scan_ids = []
error_scan_ids = []
good_scan_count = 0
invalid_scan_count = 0
error_scan_count = 0

# Feature Decomposition data:
all_sample_data = []  # one element per scan
all_sample_metadata = []  # one element per scan
energy_feature_names = np.array([])

scanlist = getscanlist()
# fileselection() # Other way to find the data with XANES spectra
xanesinseq = fileselectiontags()

scan_ids_foravg = good_scan_ids
good_scan_ids = [s for s in good_scan_ids if (any(xs in s for xs in matchers))]

# ===== Big loop for the XANES scans =====
for scan_id in good_scan_ids:
    try:
        # Read data
        print("")
        print("Scan = ", scan_id)
        energy, spectrum = readspectrum(scan_id)

        # Two + elements
        if two_plus_elem == "yes":
            cut_energy = []
            cut_spectrum = []
            for j in range(len(energy)):
                if (energy[j] >= lwr_lmt_energy) and (
                    energy[j] <= hghr_lmt_energy
                ):  # changed energy[j] <= hghr_lmt_energy)
                    cut_energy.append(energy[j])
                    cut_spectrum.append(spectrum[j])
            cut_spectrum = [float(x) for x in cut_spectrum]
            energy = cut_energy
            spectrum = cut_spectrum
            energy = np.array(energy)
            spectrum = np.array(spectrum)
            if len(energy) < 5:
                continue

        # Validate the spectrum
        if not validatespectrum():
            print("Noooooooooooooooooooooot Vaaaaaaaaaaaaaalid")
            invalid_scan_count += 1
            invalid_scan_ids.append(scan_id)
            continue

        # Interpolating spectra
        if interpolate == "yes":
            energy_range = np.arange(energy_start, energy_finish, energy_step)
            interp_func = Akima1DInterpolator(energy, spectrum)
            interpolated_spectrum = interp_func(energy_range)
            energy = energy_range
            spectrum = interpolated_spectrum

        # Normalize
        if norm_choosing == "yes":
            normspectrum, jump, preline, postline = normalization(
                energy, spectrum, edge, pre, post
            )
            spectrum = normspectrum

        # Save spectrum
        label = re.sub(r"[!@#$/\: ]", "_", scan_id)
        if save_singles == "yes":
            savedata(energy, spectrum, label)

        # Add "sample" data
        sample_data = []
        sample_metadata = [label]
        if add_tags == "yes":
            sample_metadata.extend(read_tags(scan_id))
        sample_data.extend(spectrum)
        if add_positions == "yes":
            sample_data.extend(read_positioners(scan_id))
            if norm_choosing == "yes":
                sample_data.append(jump)
        all_sample_data.append(sample_data)
        all_sample_metadata.append(sample_metadata)

        # Energy feature names
        if len(energy_feature_names) == 0:
            if two_plus_elem == "yes":
                if (
                    (hghr_lmt_energy - lwr_lmt_energy) - (energy[-1] - energy[0])
                ) < 0.001:
                    energy_feature_names = energy
            else:
                energy_feature_names = energy

        good_scan_count += 1  # To count how many files were good and were fitted

    except Exception as ex:
        print("The error exception in main loop is = ", ex)
        error_scan_count += 1
        error_scan_ids.append(scan_id)
        continue

# Loop for saving averaged spectra
if save_average == "yes":
    avgenergylist, avgspectrumlist, avgscanidlist = averaging()
    print()
    print("Number of files with multiple scans = ", len(avgenergylist))
    for i in range(len(avgenergylist)):
        try:
            # Read data
            avgenergy = avgenergylist[i]
            avgspectrum = avgspectrumlist[i]
            avg_scan_id = avgscanidlist[i]

            # Two + elements
            if two_plus_elem == "yes":
                avgcut_energy = []
                avgcut_spectrum = []
                for j in range(len(avgenergy)):
                    if (avgenergy[j] >= lwr_lmt_energy) and (
                        avgenergy[j] <= hghr_lmt_energy
                    ):
                        avgcut_energy.append(avgenergy[j])
                        avgcut_spectrum.append(avgspectrum[j])
                avgcut_spectrum = [float(x) for x in avgcut_spectrum]
                avgenergy = avgcut_energy
                avgspectrum = avgcut_spectrum
                avgenergy = np.array(avgenergy)
                avgspectrum = np.array(avgspectrum)
                if len(avgenergy) < 5:
                    continue

            # Interpolating spectra
            if interpolate == "yes":
                avgenergy_range = np.arange(energy_start, energy_finish, energy_step)
                avginterp_func = Akima1DInterpolator(avgenergy, avgspectrum)
                avginterpolated_spectrum = avginterp_func(avgenergy_range)
                avgenergy = avgenergy_range
                avgspectrum = avginterpolated_spectrum

            # Normalize
            if norm_choosing == "yes":
                avgnormspectrum, jump, preline, postline = normalization(
                    avgenergy, avgspectrum, edge, pre, post
                )
                avgspectrum = avgnormspectrum

            if matchers[0] not in avg_scan_id:
                continue

            # Save spectrum
            label = re.sub(r"[!@#$/\: ]", "_", avg_scan_id)
            label = label.split("_")
            label = [
                element for element in label[:-1]
            ]  # Removing the last number of the name
            label = "_".join(label)
            if save_singles == "yes":
                savedata(avgenergy, avgspectrum, label)

            # Add "sample" data
            sample_data = []
            sample_metadata = [label]
            if add_tags == "yes":
                sample_metadata.extend(read_tags(scan_id))
            sample_data.extend(avgspectrum)
            if add_positions == "yes":
                sample_data.extend(read_positioners(avg_scan_id))
                if norm_choosing == "yes":
                    sample_data.append(jump)
            all_sample_data.append(sample_data)
            all_sample_metadata.append(sample_metadata)

        except Exception as ex:
            print("The error exception in avg loop is = ", ex)
            error_scan_count += 1
            error_scan_ids.append(avg_scan_id[i])
            continue

# Add features as one sample to the data
feature_names = []
metadata_names = ["scan"]
if add_tags == "yes":
    metadata_names += ["sample tags", "subsample tags"]
feature_names.extend(energy_feature_names)
if add_positions == "yes":
    feature_names += ["samy", "samz", "sampy", "sampz"]
    if norm_choosing == "yes":
        feature_names.append("jump")

if len(good_scan_ids) == 0:
    print("                THE FILE SELECTION FUNCTION FAILED, good_scan_ids = 0")
if len(energy_feature_names) == 0 and (len(good_scan_ids) != 0):
    print(
        "                ADJUST THE LIMITS (lwr_lmt and hghr_lmt) TO BE INSIDE OF THE XANES SPECTRUM RANGE TO SOLVE THIS ERROR"
    )

os.makedirs(outpath, exist_ok=True)

if save_orange == "yes" or save_excel == "yes":
    index = pd.MultiIndex.from_tuples(all_sample_metadata, names=metadata_names)
    df = pd.DataFrame(all_sample_data, columns=feature_names, index=index)

    if save_orange == "yes":
        out_filename = os.path.join(outpath, orange_name + ".csv")
        df.to_csv(out_filename, float_format="%10.6f")

    if save_excel == "yes":
        out_filename = os.path.join(outpath, excel_name + ".csv")
        df.T.to_csv(out_filename, float_format="%10.6f")

    print(df)


# Prints with information of the good and bad scans
print("")
print("Good scans = ", good_scan_count)
print("Invalid scans = ", invalid_scan_count)
print("Scans with errors = ", error_scan_count)
print("")
if len(invalid_scan_ids) > 0:
    print("List of invalid scans:")
    print(invalid_scan_ids)
    print("")
if len(error_scan_ids) > 0:
    print("List of error scans:")
    pprint(error_scan_ids)
